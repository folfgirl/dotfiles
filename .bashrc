#
# ~/.bashrc
#

### Exports
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
export TERM="tmux-256color"
export EDITOR="nvim"     # $EDITOR uses neovim
export VISUAL="nvim"  # $VISUAL uses neovim 
export PATH="$PATH:$HOME/.local/bin"

    ### Aliases
	## pacman and yay aliases ##
	alias pacupd='doas pacman -Syy'
	alias pacupg='doas pacman -Syyu'
	alias pacint='doas pacman -S'
	alias pacrm='doas pacman -R'
	alias pacpur='doas pacman -Rcns'
	alias pacsear='doas pacman -Ss'
	alias paclean='doas pacman -Rns (pacman -Qtdq)'
	alias yayint='yay -S'
	alias yayrm='yay -R'
	alias yaysear='yay -Ss'
	alias yayupd='yay -Syy'
	alias yayupg='yay -Syyu'
	alias pacunlock='doas rm /var/lib/pacman/db.lck'
	alias pacuncache='doas rm /var/cache/pacman/pkg/*'

	## apt aliases ##
#	alias update='sudo apt update'
#	alias upgrade='sudo apt upgrade'
#	alias install='sudo apt install'
#	alias remove='sudo apt remove'
#	alias search='sudo apt search'
#	alias cleanup='sudo apt autoremove'
#	alias purge='sudo apt purge'

	## git aliases ##
	alias addup='git add -u'
	alias addall='git add .'
	alias branch='git branch'
	alias checkout='git checkout'
	alias clone='git clone'
	alias commit='git commit -m'
	alias fetch='git fetch'
	alias pull='git pull origin'
	alias push='git push origin'
	alias tag='git tag'
	alias newtag='git tag -a'
	alias stat='git status'
	alias remote='git remote'

	## NetworkManager aliases ##
	alias nmlist='nmcli d w'
	alias nmconnect='nmcli d w c'

	## ls aliases ##
	alias ll='ls -la --color=auto'
	alias ls='ls -a --color=auto'

	## yubikey aliases ##
	alias sha1hmac='dd status=none if=/dev/random bs=20 count=1 | xxd -p -c 40'

	# get fastest mirrors for pacman
alias mirror="doas reflector -l 30 -n 20 -p https,http --fastest 30 --threads 4 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="doas reflector -l 50 -n 20 -p https,http --sort delay --verbose --save /etc/pacman.d/mirrorlist"
alias mirrors="doas reflector -l 50 -n 20 -p https,http --sort score --verbose --save /etc/pacman.d/mirrorlist"
alias mirrora="doas reflector -l 50 -n 20 -p https,http --sort age --verbose --save /etc/pacman.d/mirrorlist"

## gpg encryption ##
# verify signature for isos
    	alias gpg-check="gpg --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
    	alias gpg-retrieve="gpg --keyserver-options auto-key-retrieve --receive-keys"

# unsets 'SSH_AGENT_PID', sets the ssh sock variable to allow gpg ssh keys
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
export GPG_TTY=$(tty)

# reinstall function 
function reinstallall {
 	pacman -Qq > .local/bin/packages.txt
	for pkgName in $(cat .local/bin/packages.txt)
		do
		doas pacman -S  --noconfirm $pkgName
	done

echo "Reinstalled all packages."
}

# Set Proxy
function setproxy() {
    export {http,https,ftp,socks}_proxy="127.0.0.1:9050"
}

# Unset Proxy
function unsetproxy() {
    unset {http,https,ftp,socks}_proxy
}

