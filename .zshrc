#
# ~/.bashrc
#

### Exports
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
export TERM="xterm-256color"
export EDITOR="emacsclient -t -a ''"     # $EDITOR use Emacs in terminal
export VISUAL="emacsclient -c -a emacs"  # $VISUAL use Emacs in gui mode



# If not running interactively, don't do anything
[[ $- != *i* ]] && return




    ### Aliases
	## pacman and yay aliases ##
	alias update='sudo pacman -Syy'
	alias upgrade='sudo pacman -Syyu'
	alias downgrade='sudo pacman -Syyuu'
	alias install='sudo pacman -S'
	alias remove='sudo pacman -R'
	alias purge='sudo pacman -Rcns'
	alias search='sudo pacman -Ss'
	alias cleanup='sudo pacman -Rns (pacman -Qtdq)'
	alias yayinstall='yay -S'
	alias yayremove='yay -R'
	alias yaysearch='yay -Ss'
	alias unlock='sudo rm /var/lib/pacman/db.lck'
	alias uncache='sudo rm /var/cache/pacman/pkg/*'

	## apt aliases ##
#	alias update='sudo apt update'
#	alias upgrade='sudo apt upgrade'
#	alias install='sudo apt install'
#	alias remove='sudo apt remove'
#	alias search='sudo apt search'
#	alias cleanup='sudo apt autoremove'
#	alias purge='sudo apt purge'

	## git aliases ##
	alias addup='git add -u'
	alias addall='git add .'
	alias branch='git branch'
	alias checkout='git checkout'
	alias clone='git clone'
	alias commit='git commit -m'
	alias fetch='git fetch'
	alias pull='git pull origin'
	alias push='git push origin'
	alias tag='git tag'
	alias newtag='git tag -a'
	alias stat='git status'
	alias remote='git remote'

	## NetworkManager aliases ##
	alias nmlist='sudo nmcli device wifi'
	alias nmconnect='sudo nmcli device wifi connect'

	## ls aliases ##
	alias ll='ls -la --color=auto'
	alias ls='ls -a --color=auto'

	# get fastest mirrors for pacman
alias mirror="sudo reflector -l 30 -n 20 -p https,http --fastest 30 --threads 4 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector -l 50 -n 20 -p https,http --sort delay --verbose --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector -l 50 -n 20 -p https,http --sort score --verbose --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector -l 50 -n 20 -p https,http --sort age --verbose --save /etc/pacman.d/mirrorlist"

        ## gpg encryption ##
# verify signature for isos
    	alias gpg-check="gpg --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
    	alias gpg-retrieve="gpg --keyserver-options auto-key-retrieve --receive-keys"

        PS1='[\u@\h \W]\$ '
    . "$HOME/.cargo/env"

[ -f "/home/folfgirl/.ghcup/env" ] && source "/home/folfgirl/.ghcup/env" # ghcup-env
