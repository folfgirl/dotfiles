
### EXPORTS
set VISUAL nvim
set fish_greeting                                 # Supresses fish's intro message
set -gx VOLTA_HOME "$HOME/.volta"
set -gx PATH "$VOLTA_HOME/bin" $PATH
set TERM "xterm-256color"                         # Sets the terminal type

### FUNCTIONS

# Set Proxy
function setproxy
    set -gx {http,https,ftp,socks}_proxy "127.0.0.1:9050"
end

# Unset Proxy
function unsetproxy
   set -e {http,https,ftp,socks}_proxy
end

### SPARK
set -g spark_version 1.0.0

complete -xc spark -n __fish_use_subcommand -a --help -d "Show usage help"
complete -xc spark -n __fish_use_subcommand -a --version -d "$spark_version"
complete -xc spark -n __fish_use_subcommand -a --min -d "Minimum range value"
complete -xc spark -n __fish_use_subcommand -a --max -d "Maximum range value"

function spark -d "sparkline generator"
    if isatty
        switch "$argv"
            case {,-}-v{ersion,}
                echo "spark version $spark_version"
            case {,-}-h{elp,}
                echo "usage: spark [--min=<n> --max=<n>] <numbers...>  Draw sparklines"
                echo "examples:"
                echo "       spark 1 2 3 4"
                echo "       seq 100 | sort -R | spark"
                echo "       awk \\\$0=length spark.fish | spark"
            case \*
                echo $argv | spark $argv
        end
        return
    end

    command awk -v FS="[[:space:],]*" -v argv="$argv" '
        BEGIN {
            min = match(argv, /--min=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
            max = match(argv, /--max=[0-9]+/) ? substr(argv, RSTART + 6, RLENGTH - 6) + 0 : ""
        }
        {
            for (i = j = 1; i <= NF; i++) {
                if ($i ~ /^--/) continue
                if ($i !~ /^-?[0-9]/) data[count + j++] = ""
                else {
                    v = data[count + j++] = int($i)
                    if (max == "" && min == "") max = min = v
                    if (max < v) max = v
                    if (min > v ) min = v
                }
            }
            count += j - 1
        }
        END {
            n = split(min == max && max ? "▅ ▅" : "▁ ▂ ▃ ▄ ▅ ▆ ▇ █", blocks, " ")
            scale = (scale = int(256 * (max - min) / (n - 1))) ? scale : 1
            for (i = 1; i <= count; i++)
                out = out (data[i] == "" ? " " : blocks[idx = int(256 * (data[i] - min) / scale) + 1])
            print out
        }
    '
end
### END OF SPARK

# Spark functions
function letters
    cat $argv | awk -vFS='' '{for(i=1;i<=NF;i++){ if($i~/[a-zA-Z]/) { w[tolower($i)]++} } }END{for(i in w) print i,w[i]}' | sort | cut -c 3- | spark | lolcat
    printf  '%s\n' 'abcdefghijklmnopqrstuvwxyz'  ' ' | lolcat
end

function commits
    git log --author="$argv" --format=format:%ad --date=short | uniq -c | awk '{print $1}' | spark | lolcat
end


# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
# The bindings for !! and !$
if [ "$fish_key_bindings" = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

# Function for creating a backup file
# ex: backup file.txt
# result: copies file as file.txt.bak
function backup --argument filename
    cp $filename $filename.bak
end

# Function for copying files and directories, even recursively.
# ex: copy DIRNAME LOCATIONS
# result: copies the directory and all of its contents.
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

function reinstallall
 pacman -Qq > .local/bin/packages.txt
    for pkgName in $(cat .local/bin/packages.txt)
        doas pacman -S  --noconfirm $pkgName
    end
 echo "Reinstalled all packages."
end

### END OF FUNCTIONS

### Aliases

## custom aliases ##

alias change-wallpaper='feh --bg-scale -z $HOME/wallpapers/*'
alias backup-music='tar -cf music.tar Music/; 
shasum -a 256 music.tar | tee -a shasum.sha;  
XZ_OPT='-9' tar -cjf "Music Lib Backup.txz" music.tar shasum.sha'
alias sha1hmac='dd status=none if=/dev/random bs=20 count=1 | xxd -p -c 40'

## yubikey aliases ##

## monero-cli aliases ##
	alias monero-cli='monero-wallet-cli --wallet-file'

## super user (root) aliases 
	alias sudo='sudo --'
	alias doas='doas --'

## pacman and yay aliases ##
	alias pacupd='doas pacman -Syy'
	alias pacupg='doas pacman -Syyu'
	alias pacint='doas pacman -S'
	alias pacrm='doas pacman -R'
	alias pacpur='doas pacman -Rcns'
	alias pacsear='doas pacman -Ss'
	alias paclean='doas pacman -Rns (pacman -Qtdq)'
	alias yayint='yay -S'
	alias yayrm='yay -R'
	alias yaysear='yay -Ss'
	alias yayupd='yay -Syy'
	alias yayupg='yay -Syyu'
	alias pacunlock='doas rm /var/lib/pacman/db.lck'
	alias pacuncache='doas rm /var/cache/pacman/pkg/*'

## apt aliases ##
	# alias update='sudo apt update'
	# alias upgrade='sudo apt upgrade'
	# alias install='sudo apt install'
	# alias remove='sudo apt remove'
	# alias search='sudo apt search'
	# alias cleanup='sudo apt autoremove'
	# alias purge='sudo apt purge'

## git aliases ##
	alias addup='git add -u'
	alias addall='git add .'
	alias branch='git branch'
	alias checkout='git checkout'
	alias clone='git clone'
	alias commit='git commit -m'
	alias fetch='git fetch'
	alias pull='git pull origin'
	alias push='git push origin'
	alias tag='git tag'
	alias newtag='git tag -a'
	alias stat='git status'
	alias remote='git remote'
	alias Switch='git switch'

## NetworkManager aliases ##
	alias nmlist=' nmcli d w'
	alias nmconnect='nmcli d w c'

## spark aliases ##
	alias clear='echo -en "\x1b[2J\x1b[1;1H" ; echo; echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo'

## navgation aliases ##
	alias .1='cd ..'
	alias .2='cd ../..'
	alias .3='cd ../../..'
	alias .4='cd ../../../..'
    	alias .5='cd ../../../../..'


## confirm before overwriting something ##
    alias cp="cp -i"
    alias mv='mv -i'
    alias rm='rm -i'

## ls aliases ##
	alias ls='ls -a --color=auto'

## gpg encryption ##
# verify signature for isos
    	alias gpg-check="gpg --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
    	alias gpg-retrieve="gpg --keyserver-options auto-key-retrieve --receive-keys"
# unsets 'SSH_AGENT_PID', sets the ssh sock variable to allow gpg ssh keys
set -e SSH_AGENT_PID
if test -z $gnupg_SSH_AUTH_SOCK_BY; or test $gnupg_SSH_AUTH_SOCK_BY -ne $fish_pid
    set -gx SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
end
set -gx GPG_TTY (tty)

# get fastest mirrors
alias mirror="doas reflector -l 30 -n 20 -p https,http --fastest 30 --threads 4 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="doas reflector -l 50 -n 20 -p https,http --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="doas reflector -l 50 -n 20 -p https,http --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="doas reflector -l 50 -n 20 -p https,http --sort age --save /etc/pacman.d/mirrorlist"

zoxide init fish | source
