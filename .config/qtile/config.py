# -*- coding: utf-8 -*-
import os
import socket
import subprocess
from typing import List
from libqtile import layout, bar, widget, hook, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.dgroups import simple_key_binder

mod = "mod4"  # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"  # My terminal of choice using my shell of choice
myBrowser = "librewolf"  # My browser of choice
mypassmanager = "keepassxc"  # My password manager of choice
keys = [
    # The essentials
    Key([mod], "c",
        lazy.window.kill(),
        desc="Kill focused window"
        ),
    Key([mod, "control"], "r",
        lazy.restart(),
        desc="Restarts qtile"
        ),
    Key([mod, "control"], "q",
        lazy.shutdown(),
        desc="Shutdown Qtile"
        ),
    Key([mod], "r",
        lazy.reload_config(),
        desc="restarts the config"
        ),
    Key([mod], "Return",
        lazy.spawn(myTerm),
        desc='runs fish in alacritty'
        ),
    Key([mod], "b",
        lazy.spawn(myBrowser),
        desc='runs librewolf'
        ),
    Key([mod], "x",
        lazy.spawn(mypassmanager),
        desc='runs keepassxc'
        ),
    Key([mod], "q",
        lazy.spawn("qpwgraph"),
        desc='runs qpwgraph'
        ),
    Key([mod], "Tab",
        lazy.next_layout(),
        desc="Toggle between layouts"
        ),
    # Treetab controls
    Key([mod, "shift"], "h",
        lazy.layout.move_left(),
        desc='Move up a section in treetab'
        ),
    Key([mod, "shift"], "l",
        lazy.layout.move_right(),
        desc='Move down a section in treetab'
        ),
    # Window controls
    Key([mod], "j",
        lazy.layout.down(),
        desc='Move focus down in current stack pane'
        ),
    Key([mod], "k",
        lazy.layout.up(),
        desc='Move focus up in current stack pane'
        ),
    Key([mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc='Move windows up in current stack'
        ),
    Key([mod], "h",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
        ),
    Key([mod], "l",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'
        ),
    Key([mod], "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'
        ),
    Key([mod], "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'
        ),
    Key([mod, "shift"], "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'
        ),
    Key([mod], "f",
        lazy.window.toggle_fullscreen(),
        desc='toggle fullscreen'
        ),
    # Stack controls
    Key([mod, "shift"], "Tab",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'
        ),
    Key([mod], "space",
        lazy.layout.next(),
        desc='Switch window focus to other pane(s) of stack'
        ),
    Key([mod, "shift"], "space",
        lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'
        ),
    # dmenu integration
    Key([mod], 'd', lazy.run_extension(extension.DmenuRun(
        dmenu_prompt="$",
        font="terminus",
        fontsize=12,
        background="#15181a",
        foreground="#00ff00",
        selected_background="#079822",
        selected_foreground="#fff",
        dmenu_lines=24,
        dmenu_bottom=False,
        ))),
]

groups = [Group("1", layout='MonadWide'),
          Group("2", layout='MonadWide', matches=[Match(wm_class=myBrowser)]),
          Group("3", layout='MonadWide'),
          Group("4", layout='MonadWide'),
          Group("5", layout='monadtall'),
          Group("6", layout='monadtall'),
          Group("7", layout='monadtall'),
          Group("8", layout='monadtall'),
          Group("9", layout='monadtall'),
          Group("10", layout='monadtall', matches=[Match(wm_class=mypassmanager)])]

# Allow MODKEY+[0 through 9] to bind to groups,
# see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
dgroups_key_binder = simple_key_binder("mod4")

layout_theme = {"border_width": 2,
                "margin": 8,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }

layouts = [
        layout.MonadWide(**layout_theme),
        layout.MonadTall(**layout_theme),
        layout.RatioTile(**layout_theme),
        layout.VerticalTile(**layout_theme),
        layout.RatioTile(**layout_theme),
        layout.Columns(**layout_theme),
        layout.Matrix(**layout_theme),
        layout.Zoomy(**layout_theme),
        layout.Bsp(**layout_theme),
        layout.Tile(shift_windows=True, **layout_theme),
        layout.Stack(num_stacks=2),
        layout.Floating()
        ]

colors = [["#282c34", "#282c34"],
          ["#1c1f24", "#1c1f24"],
          ["#dfdfdf", "#dfdfdf"],
          ["#ff6c6b", "#ff6c6b"],
          ["#98be65", "#98be65"],
          ["#da8548", "#da8548"],
          ["#51afef", "#51afef"],
          ["#c678dd", "#c678dd"],
          ["#46d9ff", "#46d9ff"],
          ["#a9a1e1", "#a9a1e1"],
          ["#ff00ff", "#ff00ff"]]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

# DEFAULT WIDGET SETTINGS #
widget_defaults = dict(
    font="Ubuntu Bold",
    fontsize=10,
    padding=2,
    background=colors[0]
)
extension_defaults = widget_defaults.copy()


def init_widgets_top_list():
    widgets_top_list = [
        widget.Sep(
            linewidth=0,
            padding=6,
            foreground=colors[2],
            background=colors[0]
        ),
        widget.Image(
            filename="~/.config/qtile/icons/python.png",
            scale="False",
        ),
        widget.Sep(
            linewidth=0,
            padding=6,
            foreground=colors[2],
            background=colors[0]
        ),
        widget.GroupBox(
            font="Ubuntu Bold",
            fontsize=12,
            margin_y=3,
            margin_x=0,
            padding_y=5,
            padding_x=3,
            borderwidth=3,
            active=colors[2],
            inactive=colors[7],
            rounded=False,
            highlight_color=colors[1],
            highlight_method="line",
            this_current_screen_border=colors[6],
            this_screen_border=colors[4],
            other_current_screen_border=colors[6],
            other_screen_border=colors[4],
            foreground=colors[2],
            background=colors[0]
        ),
        widget.TextBox(
            text='|',
            font="Ubuntu Mono",
            background=colors[0],
            foreground='474747',
            padding=0,
            fontsize=14
        ),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            foreground=colors[2],
            background=colors[0],
            padding=0,
            scale=0.7
        ),
        widget.CurrentLayout(
            foreground=colors[2],
            background=colors[0],
            padding=0,
        ),
        widget.TextBox(
            text='|',
            font="Ubuntu Mono",
            background=colors[0],
            foreground='474747',
            padding=0,
            fontsize=14
        ),
        widget.Sep(
            linewidth=0,
            padding=6,
            foreground=colors[0],
            background=colors[0]
        ),
        widget.Prompt(
            foreground=colors[2],
            background=colors[0],
            padding=5,
        ),
        widget.WindowName(
            foreground=colors[10],
            background=colors[0],
            font="terminus",
            padding=5,
        ),
        widget.Spacer(),
        widget.Systray(),
    ]
    return widgets_top_list


def init_widgets_bottom_list():
    widgets_bottom_list = [
        widget.Spacer(),
        widget.TextBox(
            text='',
            font="Ubuntu Mono",
            background=colors[0],
            foreground=colors[4],
            padding=-6,
            fontsize=37
        ),
        widget.ThermalSensor(
            foreground=colors[1],
            background=colors[4],
            threshold=90,
            fmt='Temp: {}',
            padding=5
        ),
        widget.TextBox(
            text='',
            font="Ubuntu Mono",
            background=colors[4],
            foreground=colors[5],
            padding=-6,
            fontsize=37
        ),
        widget.CheckUpdates(
            update_interval=300,
            distro="Arch_checkupdates",
            display_format="Updates: {updates} ",
            foreground=colors[1],
            colour_have_updates=colors[1],
            colour_no_updates=colors[1],
            padding=5,
            background=colors[5]
        ),
        widget.TextBox(
            text='',
            font="Ubuntu Mono",
            background=colors[5],
            foreground=colors[6],
            padding=-6,
            fontsize=37
        ),
        widget.Memory(
            foreground=colors[1],
            background=colors[6],
            fmt='Mem: {}',
            padding=5
        ),
        widget.TextBox(
            text='',
            font="Ubuntu Mono",
            background=colors[6],
            foreground=colors[7],
            padding=-6,
            fontsize=37
        ),
        widget.Backlight(
            backlight_name='intel_backlight',
            foreground=colors[1],
            background=colors[7],
            change_command=None,
            padding=5,
            brightness_file='/sys/class/backlight/intel_backlight/brightness',
            max_brightness_file=(
                '/sys/class/backlight/intel_backlight/max_brightness'),
            fmt='Brightness: {}',
        ),
        widget.TextBox(
            text='',
            font="Ubuntu Mono",
            background=colors[7],
            foreground=colors[8],
            padding=-6,
            fontsize=37
        ),
        widget.Battery(
            foreground=colors[1],
            background=colors[8],
            padding=5,
        ),
        widget.TextBox(
            text='',
            font="Ubuntu Mono",
            background=colors[8],
            foreground=colors[9],
            padding=-6,
            fontsize=37
        ),
        widget.Clock(
            foreground=colors[1],
            background=colors[9],
            format="%Y-%m-%d %a %I:%M %p"
        ),
        widget.TextBox(
            text='',
            font="Ubuntu Mono",
            background=colors[9],
            foreground=colors[10],
            padding=-6,
            fontsize=37
        ),
        widget.CPU(
            foreground=colors[1],
            background=colors[10],
            padding=0,
        ),

    ]
    return widgets_bottom_list


def init_widgets_top_bar():
    widgets_top_bar = init_widgets_top_list()
    return widgets_top_bar


def init_widgets_bottom_bar():
    widgets_bottom_bar = init_widgets_bottom_list()
    return widgets_bottom_bar


def init_screens():
    return [
            Screen(
                top=bar.Bar(
                    widgets=init_widgets_top_bar(), opacity=1.0, size=20),
                bottom=bar.Bar(
                 widgets=init_widgets_bottom_bar(), opacity=1.0, size=20)
             ),
            Screen(
                top=bar.Bar(
                    widgets=init_widgets_top_bar(), opacity=1.0, size=20),
                bottom=bar.Bar(
                    widgets=init_widgets_bottom_bar(), opacity=1.0, size=23)
                ),
            ]


if __name__ in ["config", "main"]:
    screens = init_screens()
    widgets_top_list = init_widgets_top_list()
    widgets_top_bar = init_widgets_top_bar()
    widgets_bottom_list = init_widgets_bottom_list()
    widgets_bottom_bar = init_widgets_bottom_bar()


def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_app_rules = [list]  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    # default_float_rules include:
    # utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),  # tastyworks exit box
    Match(title='Qalculate!'),  # qalculate-gtk
    Match(wm_class='kdenlive'),  # kdenlive
    Match(wm_class='pinentry-gtk-2'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/Scripts/autostart.sh'])

