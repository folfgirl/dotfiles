#!/usr/bin/env bash 

xscreensaver &
picom &
nm-applet &
blueman-applet &
# qbittorrent &
flameshot &
volumeicon &
keepassxc &
virt-manager &
# qpwgraph &
# mullvad-vpn &
# yubioath-desktop &
# indicator-stickynotes &
 tmux new -s monerod -d \; send-keys "monerod --config-file /home/$USER/dotfiles/etc/monerod/monerod_personal.conf" C-m 

# Uncomment to set wallpaper with nitrogen or feh
# nitrogen --random &
feh --randomize --bg-fill $HOME/.wallpapers/*
