vim.g.mapleader = " "
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
vim.keymap.set("n", "<leader>y", vim.cmd.Lazy)
vim.keymap.set("n", "<leader>t", vim.cmd.tabnew)

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup('plugins')

keys = {

{"<leader>ps", "<cmd>lua require('telescope.builtin').grep_string(search = vim.fn.input('Grep > '))<cr>", desc = "searches for works with grep"}
}
