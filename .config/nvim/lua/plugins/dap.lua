return {
	'mfussenegger/nvim-dap',
	dependencies = {
		'rcarriga/nvim-dap-ui',
		'jay-babu/mason-nvim-dap.nvim',
		build = function()
			pcall(vim.cmd, 'MasonUpdate')
		end,
	},
}
