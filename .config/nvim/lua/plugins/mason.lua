return {
	'williamboman/mason.nvim',
	dependencies = {
		'neovim/nvim-lspconfig',
		'williamboman/mason-lspconfig.nvim',
	},
	config = function()
		vim.keymap.set("n", "<leader>m",vim.cmd.Mason)
	end
}
