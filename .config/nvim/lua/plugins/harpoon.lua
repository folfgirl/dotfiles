return {
	'theprimeagen/harpoon',
	dependencies = { 'nvim-lua/plenary.nvim',
	},
	keys = {
		{"<leader>e", "<cmd>lua require('harpoon.ui').toggle_quick_menu()<cr>", desc = "toggles quick menu"},
		{"<leader>a", "<cmd>lua require('harpoon.mark').add_file()<cr>", desc = "adds a file to harpoon"},
		{"<leader>h", "<cmd>lua require('harpoon.ui').nav_file(1)<cr>", desc = "switches to the first file in harpoon"},
		{"<leader>j", "<cmd>lua require('harpoon.ui').nav_file(2)<cr>", desc = "switches to the second file in harpoon"},
		{"<leader>k", "<cmd>lua require('harpoon.ui').nav_file(3)<cr>", desc = "switches to the third file in harpoon"},
		{"<leader>l", "<cmd>lua require('harpoon.ui').nav_file(4)<cr>", desc = "switches to the forth file in harpoon"},
	},


}
