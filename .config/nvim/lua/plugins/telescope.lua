return {
	'nvim-telescope/telescope.nvim',
	branch = '0.1.x',
	dependencies = { 'nvim-lua/plenary.nvim',
},
	keys = {
		{"<leader>pf", "<cmd>lua require('telescope.builtin').find_files()<cr>", desc = "search for files in the current directory"},
		{"<C-p>", "<cmd>lua require('telescope.builtin').git_files()<cr>", desc = "search for files in the currentgit directory"},
	},
	-- {"<leader>ps", "<cmd>lua require('telescope.builtin').grep_string({search = vim.fn.input('Grep > ')})<cr>", ""},
}
