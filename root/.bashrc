#
# ~/.bashrc
#

### Exports
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
export TERM="xterm-256color"
export EDITOR="emacsclient -t -a ''"     # $EDITOR use Emacs in terminal
export VISUAL="emacsclient -c -a emacs"  # $VISUAL use Emacs in gui mode


    ### Aliases
	## pacman and yay aliases ##
	alias update='pacman -Syy'
	alias upgrade='pacman -Syyu'
	alias install='pacman -S'
	alias remove='pacman -R'
	alias purge='pacman -Rcns'
	alias search='pacman -Ss'
	alias cleanup='pacman -Rns (pacman -Qtdq)'
	alias yayinstall='yay -S'
	alias yayremove='yay -R'
	alias yaysearch='yay -Ss'
	alias yayupdate='yay -Syy'
	alias yayupgrade='yay -Syyu'
	alias unlock='rm /var/lib/pacman/db.lck'
	alias uncache='rm /var/cache/pacman/pkg/*'

	## apt aliases ##
#	alias update='apt update'
#	alias upgrade='apt upgrade'
#	alias install='apt install'
#	alias remove='apt remove'
#	alias search='apt search'
#	alias cleanup='apt autoremove'
#	alias purge='apt purge'

	## git aliases ##
	alias addup='git add -u'
	alias addall='git add .'
	alias branch='git branch'
	alias checkout='git checkout'
	alias clone='git clone'
	alias commit='git commit -m'
	alias fetch='git fetch'
	alias pull='git pull origin'
	alias push='git push origin'
	alias tag='git tag'
	alias newtag='git tag -a'
	alias stat='git status'
	alias remote='git remote'

	## NetworkManager aliases ##
	alias nmlist='nmcli device wifi'
	alias nmconnect='nmcli device wifi connect'

	## ls aliases ##
	alias ll='ls -la --color=auto'
	alias ls='ls -a --color=auto'

	# get fastest mirrors for pacman
alias mirror="reflector -l 30 -n 20 -p https,http --fastest 30 --threads 4 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="reflector -l 50 -n 20 -p https,http --sort delay --verbose --save /etc/pacman.d/mirrorlist"
alias mirrors="reflector -l 50 -n 20 -p https,http --sort score --verbose --save /etc/pacman.d/mirrorlist"
alias mirrora="reflector -l 50 -n 20 -p https,http --sort age --verbose --save /etc/pacman.d/mirrorlist"

        ## gpg encryption ##
# verify signature for isos
    	alias gpg-check="gpg --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
    	alias gpg-retrieve="gpg --keyserver-options auto-key-retrieve --receive-keys"

function reinstallall {
 pacman -Qq > .local/bin/packages.txt
	for pkgName in $(cat .local/bin/packages.txt)
	do
	  sudo pacman -S  --noconfirm $pkgName
	done
echo "Reinstalled all packages."
}
